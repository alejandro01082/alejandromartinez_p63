import javax.swing.JOptionPane;

public class Coche {
	private String matricula = "No matriculado";
	private String marca = "SEAT";
	private String modelo = "ALTEA";
	private String color = "Blanco";
	private boolean techoSolar = false;
	private float kilometros = 0;
	private int numPuertas = 3;
	private int numPlazas = 5;
	
	public String getMatricula() {

		return matricula;
	}
	
	public void setMatricula(String mat) {
		matricula = mat;
	}
	
	public String getMarca() {
		return marca;
	}
	
	public void setMarca(String marca) {
		this.marca = marca;
	}
	
	public String getModelo() {
		return modelo;
	}
	
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	
	public String getColor() {
		return color;
	}
	
	public void setColor(String color) {
		this.color = color;
	}
	
	public String getTechoSolar() {
		if(techoSolar) {
			return "Si";
		}else 
		{
			return "No";
		}
		
	}
	
	public void setTechoSolar(String respuesta) {
		switch(respuesta)
		{
			case "si" : techoSolar = true ; break;
			case "Si" : techoSolar = true ; break;
			case "s" : techoSolar = true; break;
			case "S" : techoSolar = true; break;
			case "SI" : techoSolar = true; break;
			default : techoSolar = false; break;
		}
	}
	
	public float getKilometros() {
		return kilometros;
	}
	
	public void setKilometros(Float km) {
		int x = 0;
		while(x == 0) 
		{
			try 
			{
				if(km < 0) 
				{
					km = Float.parseFloat(JOptionPane.showInputDialog("Ha introducido una cifra incorrecta, introduzca un número positivo"));
				}
				else 
				{
					kilometros = km;
					x++;
				}
			}catch(Exception e) {
				JOptionPane.showMessageDialog(null, "Ha introducido un número de kilómetros erróneo");
			}
			
		}
	}
	public int getNumPuertas() {
		return numPuertas;
	}
	
	public void setNumPuertas(int puertas) {
		int x = 0;
		while(x == 0) 
		{
			try {
					if(puertas < 0 || puertas > 5) 
					{
						puertas = Integer.parseInt(JOptionPane.showInputDialog("Ha introducido un número de puertas incorrecto, introduzca un número entre 1 y 5."));
					}
					else 
					{
						numPuertas = puertas;
						x++;
					}
				}
			catch(Exception e){
				JOptionPane.showMessageDialog(null,"Ha introducido un número de puertas incorrecto");
			}
		}
	}
	
	public int getNumPlazas() {
		return numPlazas;
	}
	
	public void setNumPlazas(int plazas) {
		int x = 0;
		while(x == 0) 
		{
			try {
					if(plazas < 0 || plazas > 7) 
					{
						plazas = Integer.parseInt(JOptionPane.showInputDialog("Ha introducido un número de puertas incorrecto, introduzca un número entre 1 y 5."));
					}
					else 
					{
						numPlazas = plazas;
						x++;
					}
				}
			catch(Exception e){
				JOptionPane.showMessageDialog(null,"Ha introducido un número de puertas incorrecto");
			}
		}
	}
	
	public void avanzar(float km) {
		int x = 0;
		while(x == 0) 
		{
			try 
			{
				if(km > 0) 
				{
					kilometros = kilometros + km;
					x++;
					JOptionPane.showMessageDialog(null, "Hemos sumado dicha cantidad del kilómetros al cuentakilómetros." + "\n" + "Kilómetros totales: " + kilometros);
				}else 
				{
					km = Float.parseFloat(JOptionPane.showInputDialog("Ha introducido una cantidad de kilómetros errónea, introduzca una cantidad correcta"));
				}
				
			}
			catch(Exception e)
			{
				km = Float.parseFloat(JOptionPane.showInputDialog("Ha introducido una cantidad de kilómetros errónea, introduzca una cantidad correcta"));
			}
		}
	}
	
	public void tunear()
	{
		kilometros = 0;
		if(techoSolar) {
			JOptionPane.showMessageDialog(null, "Se ha reseteado el cuentakilómetros a 0");
		}else {
			JOptionPane.showMessageDialog(null, "Se ha instalado el techo solar y se ha reseteado el cuentakilómetros a 0");
			techoSolar = true;
		}
	}
	
	public void tunear(String color) 
	{
		tunear();
		this.color = color;
		JOptionPane.showMessageDialog(null, "Además de los cambios anteriores, hemos cambiado el color del coche a " + this.color);
	}
	
	public void matricular(String matricula) 
	{
		this.matricula = matricula;
		JOptionPane.showMessageDialog(null, "Se ha matriculado el coche. A partir de ahora tendrá la matrícula " + this.matricula);
	}
	
	
	public Coche() {
		
	}
	
	public Coche(String mat) {
		matricula = mat;
	}
	
	public Coche(int puertas, int plazas) {
		setNumPuertas(puertas);
		setNumPlazas(plazas);
	}
	
	public Coche(String marcaCoche, String modeloCoche, String colorCoche) {
		marca = marcaCoche;
		modelo = modeloCoche;
		color = colorCoche;
	}
}
