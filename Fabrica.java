import javax.swing.JOptionPane;

public class Fabrica {
	public static void main(String[] args) {
		Coche total[] = new Coche[4];
		total[0] = new Coche();
		total[0].setMatricula("1234-DF");
		total[0].setMarca("Seat");
		total[0].setModelo("Toledo");
		total[0].setColor("Azul");
		total[0].setTechoSolar("No");
		total[0].setKilometros(200F);
		total[0].setNumPuertas(3);
		total[0].setNumPlazas(5);
		
		String matricula = "5678-AG";
		total[1] = new Coche(matricula);
		total[1].setMarca("Fiat");
		total[1].setModelo("Uno");
		total[1].setColor("Rojo");
		total[1].setTechoSolar("Si");
		total[1].setKilometros(300F);
		total[1].setNumPuertas(3);
		total[1].setNumPlazas(2);
		
		int numPuertas = 5;
		int numPlazas = 5;
		total[2] = new Coche(numPuertas,numPlazas);
		total[2].setMatricula("9012-HH");
		total[2].setMarca("BMW");
		total[2].setModelo("850");
		total[2].setColor("Gris");
		total[2].setTechoSolar("no");
		total[2].setKilometros(400F);
		
		String marca, modelo, color;
		marca = "VW";
		modelo = "Caddy";
		color = "Blanco";
		total[3] = new Coche(marca, modelo, color);
		total[3].setMatricula("3456-XS");
		total[3].setTechoSolar("si");
		total[3].setKilometros(500F);
		total[3].setNumPuertas(5);
		total[3].setNumPlazas(7);
		
		for(int x = 0; x < 4; x++) 
		{
			caracteristicas(total[x]);
		}
		
		JOptionPane.showMessageDialog(null, "Voy a tunear los dos primeros coches");
		total[0].tunear();
		JOptionPane.showMessageDialog(null, "Ya se ha tuneado el primer coche");
		total[1].tunear("Morado");
		
		caracteristicas(total[0]);
		caracteristicas(total[1]);
	}
	
	public static void caracteristicas(Coche c) {
		JOptionPane.showMessageDialog(null, "Coche" + "\n" + "Matrícula:" + 
		c.getMatricula() + "\n" + "Marca:" + c.getMarca() + 
		"\n" + "Modelo:" + c.getModelo() + "\n" + 
		"Color:" + c.getColor() + "\n" + "Techo Solar:" + c.getTechoSolar() + "\n" + 
		"Km:" + c.getKilometros() + "\n" + "Nº puertas:" + c.getNumPuertas() + "\n" + 
		"Nº plazas:" + c.getNumPlazas());
	}
}
